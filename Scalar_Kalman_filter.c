#include <stdio.h>
#include "Scalar_Kalman_filter.h"

/*
** APIs for Prediction state
*/
float Predict_NewState(float fl_PreviousState,float fl_ControlVariable, float fl_StateNoise);
float Predict_ProcessCovariance(float fl_PreviousCovariance, float fl_ProcessNoise);

/*
** API for calculating Kalman gain
*/
float CalculateKalmanGain(float fl_PredictedCovariance, float fl_MeasurementError);

/*
** APIs for Correction state
*/
float Correct_NewState(float fl_PredictedNewState, float fl_ActualMeasuredState, float fl_KalmanGain );
float Correct_ProcessCovariance(float fl_KalmanGain, float fl_PredictedCovariance);

float Predict_NewState(float fl_PreviousState,float fl_ControlVariable, float fl_StateNoise)
{

  /* X-Pred(k)  = X(k-1) + U(k) + W(k) */

  float fl_PredictedNewState =  (fl_PreviousState + fl_ControlVariable + fl_StateNoise);

  return(fl_PredictedNewState);
}

float Predict_ProcessCovariance(float fl_PreviousCovariance, float fl_ProcessNoise)
{


  /* P-Pred(k)  = P(k-1) + Q(k) */

  float fl_PredictedProcessCov = (fl_PreviousCovariance +  fl_ProcessNoise);

  return(fl_PredictedProcessCov);
}


float CalculateKalmanGain(float fl_PredictedCovariance, float fl_MeasurementError)
{

  /* KG  = P(k-1) / ((P(k-1) + R ) */

  float fl_KG = (fl_PredictedCovariance)/(fl_PredictedCovariance + fl_MeasurementError);

  return(fl_KG);
}


float Correct_NewState(float fl_PredictedNewState, float fl_ActualMeasuredState, float fl_KalmanGain )
{

  /* X(k)  = X-Pred(k) + (KG  * ( Y(k) - X-Pred(k)) ) */
  
  float fl_CorrectedState = fl_PredictedNewState + (fl_KalmanGain * ( fl_ActualMeasuredState - fl_PredictedNewState ) );

  return(fl_CorrectedState);
}

float Correct_ProcessCovariance(float fl_KalmanGain, float fl_PredictedCovariance)
{

  /* P(k)  = (1-KG) * x-Pred(k) */

  float fl_CorrectedCov = (1 - fl_KalmanGain) * fl_PredictedCovariance;

  return(fl_CorrectedCov);
}
/*
**  APIs for Kalman filter Algo
*/
void KalmanFilterAlgo(float a, float b, float c, float d, float e, float f, float g, float *h, float *i);


/*
** Kalman Filter implementation
*/
void KalmanFilterAlgo(float fl_PreviousState, float fl_ControlVariable, float fl_StateNoise, float fl_PreviousCovariance, float fl_ProcessNoise, float fl_MeasurementError, float fl_ActualMeasuredState, float *l_CorrectedState, float *l_CorrectedCov)
{


  /*
  ** Predict the new state based on previous state and control variable along with state noise
  ** Predict the new process covariance using previous covariance along with process noise
  */
  float l_PredictedNewState = Predict_NewState(fl_PreviousState, fl_ControlVariable, fl_StateNoise);
  printf("Predicted New state:%f\n", l_PredictedNewState );

  float l_PredictedProcessCov =  Predict_ProcessCovariance(fl_PreviousCovariance, fl_ProcessNoise);
    printf("Predicted Process Covariance:%f\n", l_PredictedProcessCov );


  /*
  ** Calculate the Kalman gain based on predicted covariance and measurement error
  */
  float l_KG = CalculateKalmanGain(l_PredictedProcessCov, fl_MeasurementError);
    printf("Kalman Gain:%f\n", l_KG );

  
  /*
  ** Correct the predicted state with Kalman gain and actual measured state.
  ** Correct the predicted covariance with kalman gain
  */
  *l_CorrectedState = Correct_NewState(l_PredictedNewState, fl_ActualMeasuredState, l_KG );
  printf("-----------------------------------\n");
  printf("Corrected State:%f\n", *l_CorrectedState );


  *l_CorrectedCov = Correct_ProcessCovariance(l_KG, l_PredictedProcessCov);
  printf("Corrected Covariance:%f\n", *l_CorrectedCov );
  printf("-----------------------------------\n");

}


void KalmanFilterIteration()
{
  
  /*
  ** Initial/Prediction state variables
  */

  float l_PreviousState = 65.0;   /* X(k-1) */
  float l_ControlVariable = 2.0;  /* U*k) */
  float l_StateNoise = 0;       /* W(k)  */

  float l_PreviousCovariance = 2.0; /* P(k-1)  */
  float l_ProcessNoise = 0;       /* Q(k)  */
  
  /*
  ** Kalman gain variables
  */
  float l_MeasurementError = 0.05;   /* R  */

  /*
  ** Actual measurement variables.
  */
  float l_measurements[] = {50.0,51.5,53.5,55.0,57.0,59.5};
  #define MAX_MEASUREMENTS  (sizeof(l_measurements)/sizeof(l_measurements[0]))
 


  /*
  ** Correction/New state variables
  */
  float l_NewState = 0.0;           /* X(k)  */
  float l_NewCov = 0.0;             /* P(k)  */

  for (int i = 0; i < MAX_MEASUREMENTS; i++)
  {

    /*
    ** for each measurement, run kalman filter until last measurement
    */
    KalmanFilterAlgo(l_PreviousState, l_ControlVariable, l_StateNoise, \
    l_PreviousCovariance, l_ProcessNoise , \
    l_MeasurementError, l_measurements[i], \
    &l_NewState, &l_NewCov);
    
    /*
    ** After each iteration, change new state and covariance to previous state and covariance
    */
    l_PreviousState = l_NewState;
    l_PreviousCovariance = l_NewCov;


  }


}
